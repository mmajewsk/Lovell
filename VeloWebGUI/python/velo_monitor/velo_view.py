import time

from flask import (
    Blueprint,
    render_template,
    g
)

from veloview.config import Config as VeloViewConfig
from veloview.core.io import DQDB
from veloview.runview import dq_trends
from velo_monitor import config

velo_view = Blueprint('velo_view', __name__,
                      template_folder='templates/velo_view')


@velo_view.route('/')
@velo_view.route('/overview')
def overview():
    """Display table of DQ summaries for recent runs."""
    db = DQDB(VeloViewConfig().dq_db_file_path, read_only=True)
    runs = db.get_runs()
    new_runs = runs[-config.LATEST_RUNS:]
    dq_data = [db.read(run) for run in new_runs]

    for d in dq_data:
        d['comment'] = db.get_comment(d['runnr'])
        # Cast all scores and levels as integers for nicer displaying
        for key in d:
            if key.endswith(('lvl', 'score')):
                try:
                    d[key] = int(d[key])
                except (TypeError, ValueError):
                    # Protect against None values in the DB
                    d[key] = -1

    g.active_page = 'velo_view/overview'
    g.dq_data = dq_data

    return render_template('velo_view/overview.html')


@velo_view.route('/trends')
def trends():
    """Show change of DQ quantities over time."""
    g.active_page = 'velo_view/trends'
    g.trendables = dq_trends.get_trending_variables()
    return render_template('velo_view/trends/layout.html')
