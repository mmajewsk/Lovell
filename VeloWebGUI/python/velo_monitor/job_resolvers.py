def run_view_resolver(name):
    """Resolve a job for the run view.

    Requires the job name to start with `run_view`.
    """
    whitelist = [
        'tasks.runview_plot',
        'tasks.iv_scan_plot',
        'tasks.iv_scan_corrected_plot',
        'tasks.trending_plot',
        'runview.plots.get_run_plot_with_reference',
        'runview.dq_trends.get_dq_tree'
    ]
    if name not in whitelist:
        return None

    # Prepend task with the Python module it resides in
    if name.startswith('tasks'):
        name = 'velo_monitor.' + name
    elif name.startswith('runview'):
        name = 'veloview.' + name

    return name


def trends_resolver(name):
    """Resolve a job for the trending view."""
    if name != 'runview.plots.get_trending_plot':
        return None
    return 'veloview.{0}'.format(name)
