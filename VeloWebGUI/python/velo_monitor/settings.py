from flask import (
    Blueprint,
    render_template,
    g
)

settings = Blueprint('settings', __name__,
                     template_folder='templates/settings')

@settings.route('/')
def index():
    """Display configuration."""
    g.active_page = 'settings/index'

    return render_template('settings/index.html')
