from datetime import datetime

from flask import (
    Blueprint,
    flash,
    g,
    redirect,
    render_template,
    request,
    url_for
)

from veloview.special import iv_scans as veloview_ivs
from velo_monitor.utilities import memoize

special_analyses = Blueprint('special_analyses', __name__,
                             template_folder='templates/special_analyses')

# There are 42 modules, each with two sensors
MODULES = range(0, 42)
# Cache expiry time for checking list of available IV scans
ONE_DAY_IN_SECONDS = 24*60*60


@special_analyses.route('/')
@special_analyses.route('/index')
def index():
    """Index page for special analyses"""
    g.active_page = 'special_analyses/index'
    return render_template('special_analyses/index.html')


@special_analyses.route('/iv_scans', defaults=dict(scan=None, ref=None,
                                                   module=0))
@special_analyses.route('/iv_scans/<scan>', defaults=dict(ref=None, module=0))
@special_analyses.route('/iv_scans/<scan>/<ref>/<int:module>')
def iv_scans(scan, ref, module):
    """Display pairs of IV scans, two scans per module.

    The default nominal and reference scans are None so that we can dynamically
    query the list of available scans for the latest that is available. If we
    were to use _default_iv_scan directly in the route decorator, it would only
    be executed when this method is parsed, and so the default scan would only
    change when the whole application is reloaded.
    """
    scans, datetimes = _iv_scans_datetimes()
    do_redirect = False

    # See if the request was for a particular IV scan and redirect
    if request.args:
        scan = request.args.get('scan', scan)
        ref = request.args.get('ref', ref)
        module = request.args.get('module', module)
        do_redirect = True

    # Cast int arguments so the `arg in list_of_args` checks work
    invalid_module = False
    try:
        module = int(module)
    except (TypeError, ValueError):
        invalid_module = True

    # If no scan or an invalid scan was specified fall back to the default
    if scan is None or scan not in scans:
        do_redirect = True
        new_scan = _default_iv_scan()
        # Don't show errors for the default route, when no scan is specified
        if scan is not None:
            flash('Invalid IV scan "{0}", reset to "{1}"'.format(
                scan, new_scan
            ), 'error')
        scan = new_scan
    if ref is None or ref not in scans:
        do_redirect = True
        new_ref = _default_reference_iv_scan()
        # Don't show errors for the default route, when no ref is specified
        if ref is not None:
            flash('Invalid reference IV scan "{0}", reset to "{1}"'.format(
                ref, new_ref
            ), 'error')
        ref = new_ref
    if invalid_module or module not in MODULES:
        do_redirect = True
        new_module = 0
        flash('Invalid module "{0}", reset to "{1}"'.format(
            module, new_module
        ), 'error')
        module = new_module
    if do_redirect:
        url = url_for('special_analyses.iv_scans', scan=scan, ref=ref,
                      module=module)
        return redirect(url)

    g.scans = scans
    g.datetimes = map(_datetime_to_string, datetimes)
    g.modules = MODULES
    g.scan = scan
    g.reference = ref
    g.module = module
    g.active_page = 'special_analyses/iv_scans'

    return render_template('special_analyses/iv_scans.html')


@memoize(refresh_rate=ONE_DAY_IN_SECONDS)
def _iv_scans_datetimes():
    """Return two lists of equal length: available scan IDs, and datetimes."""
    scan_datetimes = veloview_ivs.iv_datetime_list()
    scan_datetimes = sorted(scan_datetimes, key=lambda x: x[1], reverse=True)
    return zip(*scan_datetimes)


def _default_iv_scan():
    """Return the default IV scan to show if one isn't specified."""
    # The latest scan
    return _iv_scans_datetimes()[0][0]


def _default_reference_iv_scan():
    """Return the default reference IV scan to show if one isn't specified."""
    # The second-latest scan
    return _iv_scans_datetimes()[0][1]


def _datetime_to_string(dt):
    """Format the datetime object as a string."""
    return datetime.strftime(dt, "%d %b %Y at %H:%M")
