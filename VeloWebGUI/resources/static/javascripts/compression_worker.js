/* Web worker to convert job response objects to LZ-compressed JSON strings.
 *
 * The spawning thread may call postMessage with a single object as the
 * argument, and the worker will postMessage a response as an LZ-compressed
 * JSON string.
 */
// It's not nice that this path is hard-coded, but I'm not sure of a nice way
// of having the (ideally dynamically-generated) path passed to this worker
// Could pass it as an argument to onmessage, but the importScripts will be
// called with the same argument every time the worker is used
self.importScripts('/velo_monitor/javascripts/lib/lz-string.min.js');
self.onmessage = function(ev) {
  self.postMessage(LZString.compressToUTF16(JSON.stringify(ev.data)));
};
