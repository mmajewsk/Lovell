import sys
import itertools

reload(sys)
sys.setdefaultencoding('utf8')
from PyQt4.QtGui import (
    QApplication,
    QGridLayout,
    QMainWindow,
    QTabWidget,
    QWidget,
)

import lFuncs
import lInterfaces
import lTab
import lTabOptions
from lovellCalibrationWidget import CalibrationView

from veloview.runview import utils

class lovellGui(QMainWindow):
    def __init__(self, run_data_dir, iv_data_dir,calibration_data_dir=None, parent=None):
        QMainWindow.__init__(self, parent)
        self.top_tab = QTabWidget(self)
        self.setCentralWidget(self.top_tab)
        self.top_tab.addTab(velo_view(run_data_dir, self), 'VELO view')
        self.top_tab.addTab(run_view(run_data_dir, self), 'Run view')
        self.top_tab.addTab(sensor_view(run_data_dir, iv_data_dir, self), 'Sensor view')
        self.top_tab.addTab(CalibrationView(calibration_data_dir, self), 'TELL1')
        self.top_tab.addTab(special_analyses(run_data_dir, iv_data_dir, self), 'Special analyses')
        self.top_tab.setCurrentIndex(1)


class velo_view(QWidget):
    # Class to set up VELO view tab with DQ trending
    def __init__(self, run_data_dir, parent=None):
        QTabWidget.__init__(self, parent)
        self.run_data_dir = run_data_dir
        self.grid_layout = QGridLayout()
        lFuncs.setPadding(self.grid_layout)
        self.setLayout(self.grid_layout)
        self.pages = []
        self.setup_tabs()


    def setup_tabs(self):
        self.top_tab = QTabWidget(self)
        veloview_config = lInterfaces.veloview_config()
        self.tab_options = lTabOptions.lTabOptions(self, self.run_data_dir, iv_data_dir = None)
        nTabs = len(veloview_config)
        i=1
        single_file_mode = utils.single_file_mode(self.run_data_dir)
        for key, val in veloview_config.iteritems():
            skip_trending_plots = single_file_mode and val['title'] in ['Trends', 'Detailed trends']
            if skip_trending_plots : continue
            msg = 'Preparing tab (' + str(i) + '/' + str(nTabs) + '): ' + val['title']
            print msg
            page = lTab.lTab(val, self)
            if val['title'] == 'Trends': page.modifyPageForTrending(self.run_data_dir,False) 
            if val['title'] == 'Detailed trends': page.modifyPageForTrending(self.run_data_dir,True)
            self.top_tab.addTab(page, val['title'])
            self.pages.append(page)
            i+=1

        self.grid_layout.addWidget(self.top_tab, 0, 0)
        self.top_tab.currentChanged.connect(self.tab_changed)

        if self.tab_options:
            self.tab_options.state_change.connect(self.tab_changed)
            self.grid_layout.addWidget(self.tab_options, 1, 0)
            # Keep options stored but don't show corresponding widget in GUI
            self.tab_options.setVisible(False)
        self.tab_changed()

    def tab_changed(self):
        iPage = self.top_tab.currentIndex()
        self.pages[iPage].replotTrend(self.tab_options.state())

class run_view(QWidget):
    def __init__(self, run_data_dir, parent=None):
        QTabWidget.__init__(self, parent)
        print 'run-data-dir set to:', run_data_dir
        self.run_data_dir = run_data_dir
        self.grid_layout = QGridLayout()
        lFuncs.setPadding(self.grid_layout)
        self.setLayout(self.grid_layout)
        self.pages = []
        self.setup_tabs()


    def setup_tabs(self):
        self.top_tab = QTabWidget(self)
        runview_config = lInterfaces.runview_config()

        # Do the sensor overview adjustments here.
        self.prepSensorOverview(runview_config)
        i=1
        self.tab_options = lTabOptions.lTabOptions(self, self.run_data_dir, iv_data_dir = None)
        nTabs = len(runview_config)
        for key, val in runview_config.iteritems():
            msg = 'Preparing tab (' + str(i) + '/' + str(nTabs) + '): ' + val['title']
            print msg
            page = lTab.lTab(val, self)
            self.top_tab.addTab(page, val['title'])
            self.pages.append(page)
            i+=1

        self.grid_layout.addWidget(self.top_tab, 0, 1)
        self.top_tab.currentChanged.connect(self.tab_changed)

        self.tab_options.state_change.connect(self.tab_changed)
        self.grid_layout.addWidget(self.tab_options, 0, 0)
        msg = "Current run number: " + self.tab_options.state().runNum
        self.tab_options.notify(msg)
        self.tab_changed()


    def tab_changed(self):
        iPage = self.top_tab.currentIndex()
        self.pages[iPage].replot(self.tab_options.state(), self.tab_options)


    def prepSensorOverview(self, config):
        # Find the entry in the dictionary for the sensor overview.
        # Just dictionary gymnastics.
        for key, val in config.iteritems():
            if key == 'sensor_overview':
                plots = []
                # Now loop over all plots in the other tabs (inc. subtabs).
                for key2, val2 in config.iteritems():
                    if key2 == 'sensor_overview': continue
                    if 'plots' in val2: plots += self.findSensorViewPlots(val2['plots'])
                    elif 'subpages' in val2:
                        for page in val2['subpages']:
                            if 'plots' in page:
                                plots += self.findSensorViewPlots(page['plots'])


                plotsPerPage=[plots[x:x+4] for x in xrange(0, len(plots), 4)]
                subpages = []

                for i in range(len(plotsPerPage)):
                    subpages.append({'title': str(i), 'plots': plotsPerPage[i]})

                if 'subpages' in val: val['subpages'] += subpages
                else: val['subpages'] = subpages




    def findSensorViewPlots(self, plot_list):
        plots = []
        for plot in plot_list:
            if 'showInSensorOverview' in plot and plot['showInSensorOverview']:
                plots.append(plot)

        return plots

class sensor_view(QWidget):
    def __init__(self, run_data_dir, iv_data_dir, parent=None):
        QTabWidget.__init__(self, parent)
        print 'run-data-dir set to:', run_data_dir
        self.run_data_dir = run_data_dir
        self.iv_data_dir = iv_data_dir
        self.grid_layout = QGridLayout()
        lFuncs.setPadding(self.grid_layout)
        self.setLayout(self.grid_layout)
        self.pages = [] 
        self.setup_tabs()

    def setup_tabs(self):
        self.top_tab = QTabWidget(self)
        runview_config = lInterfaces.sensorview_config()
        specialanalyses_config = lInterfaces.specialanalyses_config()

        i=1
        self.tab_options = lTabOptions.lTabOptions(self, self.run_data_dir, self.iv_data_dir)
        nTabs = len(runview_config) + len(specialanalyses_config)
        for key, val in itertools.chain(runview_config.iteritems(), specialanalyses_config.iteritems()):
            msg = 'Preparing tab (' + str(i) + '/' + str(nTabs) + '): ' + val['title']
            print msg
            page = lTab.lTab(val, self)
            if val['title'] == 'IV scans': page.modifyPageForIV(self.iv_data_dir)
            self.top_tab.addTab(page, val['title'])
            self.pages.append(page)
            i+=1

        if not utils.single_file_mode(self.run_data_dir) :
            # Add sensor trending page
            self.setup_trending_tab()
            self.trend_tab_index = i - 1
        else : 
            self.trend_tab_index = None

        self.grid_layout.addWidget(self.top_tab, 0, 1)
        self.top_tab.currentChanged.connect(self.tab_changed)

        self.tab_options.state_change.connect(self.tab_changed)
        self.grid_layout.addWidget(self.tab_options, 0, 0)
        msg = "Current run number: " + self.tab_options.state().runNum
        self.tab_options.notify(msg)
        self.tab_changed()

    def setup_trending_tab(self) : 
        page = lTab.lTab({'title': 'Trends', 'plots': [{'title': 'Trend view', 'name': 'Trends', 'trending': True}]}, self)
        page.modifyPageForSensorTrending(self.run_data_dir)
        page.replotSensorTrend(self.tab_options.state())
        self.top_tab.addTab(page, 'Trends')
        self.pages.append(page)

    def tab_changed(self):
        iPage = self.top_tab.currentIndex()
        if self.trend_tab_index and iPage == self.trend_tab_index:
            self.pages[iPage].replotSensorTrend(self.tab_options.state())
        else:
            self.pages[iPage].replot(self.tab_options.state(), self.tab_options)


class special_analyses(QWidget):
    def __init__(self, run_data_dir, iv_data_dir, parent=None):
        QTabWidget.__init__(self, parent)
        print 'run-data-dir set to:', run_data_dir
        print 'iv-data-dir set to:', iv_data_dir
        self.run_data_dir = run_data_dir
        self.iv_data_dir = iv_data_dir
        self.grid_layout = QGridLayout()
        lFuncs.setPadding(self.grid_layout)
        self.setLayout(self.grid_layout)
        self.pages = [] 
        self.setup_tabs()
        

    def setup_tabs(self):
        self.top_tab = QTabWidget(self)
        specialanalyses_config = lInterfaces.specialanalyses_config()
        
        i=1
        self.tab_options = lTabOptions.lTabOptions(self, self.run_data_dir, self.iv_data_dir)
        nTabs = len(specialanalyses_config)
        for key, val in specialanalyses_config.iteritems():
            msg = 'Preparing tab (' + str(i) + '/' + str(nTabs) + '): ' + val['title']
            print msg
            page = lTab.lTab(val, self)
            if val['title'] == 'IV scans': page.modifyPageForIV(self.iv_data_dir)
            self.top_tab.addTab(page, val['title'])
            self.pages.append(page)
            i+=1
            
        self.grid_layout.addWidget(self.top_tab, 0, 1)
        self.top_tab.currentChanged.connect(self.tab_changed)
        
        self.tab_options.state_change.connect(self.tab_changed)
        self.grid_layout.addWidget(self.tab_options, 0, 0)
        msg = "Current run number: " + self.tab_options.state().runNum
        self.tab_options.notify(msg)
        self.tab_changed()

 
    def tab_changed(self):
        iPage = self.top_tab.currentIndex()
        self.pages[iPage].replot(self.tab_options.state(), self.tab_options)
        
        

def main(run_data_dir,iv_data_dir, calibration_data_dir):
    print 'Starting Lovell'
    app = QApplication([]) 
#     form = run_view(run_data_dir)
    form = lovellGui(run_data_dir,iv_data_dir, calibration_data_dir)
    form.resize(1200, 700)
    form.show() 
    app.setStyle("plastique")
    app.exec_()
