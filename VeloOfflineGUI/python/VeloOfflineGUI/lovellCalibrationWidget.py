import lFuncs
import lInterfaces
import lTab
import lTabOptions
import lCalibrationInterfaces

from PyQt4.QtGui import (
    QGridLayout,
    QTabWidget,
    QWidget,
)
import logging

logging.basicConfig(format='%(name)s:[%(levelname)s]: %(message)s')
logger = logging.getLogger('lovellCalibrationWidget.VeloOfflineGui')
logger.setLevel(logging.DEBUG)

class CalibrationView(QWidget):


    def tab_changed(self):
        page_index = self.top_tab.currentIndex()
        page = self.pages[page_index]
        tab_state = self.tab_options.state()
        page.replotSpecial(tab_state, self.tab_options)


    def __init__(self, calibration_data_dir, parent):
        logger.debug('Calibration data dir: {}'.format(calibration_data_dir))
        self.setup_qt_layouts(parent)
        self.top_tab = QTabWidget(self)

        if calibration_data_dir is not None:
            self.pages = []
            self.run_data_dir = calibration_data_dir
            runview_config = lCalibrationInterfaces.calibrationview_config()
            #self.prepSensorOverview(runview_config)
            self.setup_tabs_options()
            self.iterate_tabs(runview_config)
            self.grid_layout.addWidget(self.top_tab, 0, 1)
            self.top_tab.currentChanged.connect(self.tab_changed)
            self.tab_options.state_change.connect(self.tab_changed)
            self.grid_layout.addWidget(self.tab_options, 0, 0)
            run_number = self.tab_options.state().runNum
            msg = "Current run name: {}".format(run_number)
            self.tab_options.notify(msg)
            self.tab_changed()


    def setup_qt_layouts(self, parent):
        QTabWidget.__init__(self, parent)
        self.grid_layout = QGridLayout()
        lFuncs.setPadding(self.grid_layout)
        self.setLayout(self.grid_layout)

    def setup_tabs_options(self):
        name_labels = {
            'selector_1': "Run name:",
            'selector_2': "Reference name:",
        }
        self.tab_options = lTabOptions.lTabOptions(self,
                                                   self.run_data_dir,
                                                   interface=lCalibrationInterfaces,
                                                   name_labels=name_labels)

    def iterate_tabs(self, view_config):
        n_tabs = len(view_config)
        for i, (key, val) in enumerate(view_config.iteritems(), start=1):
            tab_title = val['title']
            logger.info('Preparing tab ({}/{}): {}'.format(i, n_tabs, tab_title))
            page = lTab.lTab(val, self)
        #page.modifyPageForTrending(self.run_data_dir)
            self.pages.append(page)
        #self.conditional_preparation(tab_title, page)
            self.top_tab.addTab(page, tab_title)