#!/usr/bin/env python
"""
 author: Maciej Witold Majewski
description:
	Retrieves Tell1 calibration parameters.
"""

import DDDB.Configuration
import os
from datetime import datetime
from time import mktime
from os import getenv
import DDDB.Configuration
from CondDBUI.Admin import DumpToFiles
import argparse
import logging
from CondDBUI import CondDB

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

directory = r"/tmp/%s/VeloView/" % getenv("USER")  # static field for main directory
tmp_dump_loc = r"/tmp/%s/VeloView/xmls" % getenv("USER")  # static field with directory for dumping xmls

db_version = 'cool'
## cool version needs this env
# VELOSQLITEDBPATH=/afs/cern.ch/lhcb/software/releases/DBASE/Det/VeloSQLDDDB/v4r9/db

def _db_format_to_datetime(db_time):
	return datetime.fromtimestamp(float(db_time)/1e9)

def _datetime_to_db_format(datetime_):
	time1 = mktime(datetime_.timetuple())
	time2 = int(time1 * 1e9)
	return time2

class DBWrapper(object):
	def __init__(self, path, connectionString, create_new_db=False, **kwargs):
		self.path = path
		print "Ignore message line above."
		self.connectionString = os.path.expandvars(connectionString)
		self.create_new_db=create_new_db

	def __enter__(self):
		self.openDatabase(self.connectionString, self.create_new_db, self.readOnly)
		return self

	def __exit__(self, type, value, traceback):
		self.closeDatabase()

try:
	from CondDBUI.GitBridge import CondDB as GitBridgeCondDB
	class GitDBWrapper(GitBridgeCondDB,  DBWrapper):
		def __init__(self, *args, **kwargs):
			GitBridgeCondDB.__init__(self, "", **kwargs)
			DBWrapper.__init__(self, *args, **kwargs)
except ImportError as e:
	logger.debug('import error: {}'.format(e))
	class GitDBWrapper(object):
		pass


class CondDBWrapper(CondDB,  DBWrapper):
	def __init__(self, path, *args, **kwargs):
		CondDB.__init__(self, "", **kwargs)
		connection_string = 'sqlite_file:{}/VELOCOND'.format(path)
		DBWrapper.__init__(self, path, connection_string, *args, **kwargs)

def produce_db_object(db_version):
	if db_version == 'git':
		path = DDDB.Configuration.GIT_CONDDBS['VELOCOND']
		return GitDBWrapper(path)
	else:
		path = os.getenv('VELOSQLITEDBPATH')
		path += '/VELOCOND.db'
		return CondDBWrapper(path)

def move_to_dir(tempath1, destination):
	os.system('mv ' + tempath1 + '/VeloCondDB/TELL1/*.xml' + ' ' + tempath1)
	os.system('cd ' + tempath1 + ' ; rm -rf lhcb.xml StaticDBConfig VeloCondDB VELOCOND.db Tell1Map.xml')
	os.system('mv ' + tempath1 + ' ' + destination)
	clean(tempath1)

def create_xml_dir(directory):
	if not os.path.exists(directory): #creating directory for xmls
		os.umask(0)
		os.mkdir(directory, 0777)

def dump_to_dir(destination, verbose=False, tag='HEAD'):
	create_xml_dir(directory)
	with produce_db_object(db_version) as db:
		version_tag = tag
		from_time = 1
		to_time = _datetime_to_db_format(datetime.now())
		temp_db = db.getPayloadList('/VeloCondDB/TELL1/VeloTELL1Board0.xml', from_time, to_time, tag=version_tag)
		calibration_dates_ints = [since for payload, since, until, chID, insertTime in temp_db]
		first_calibration_date = datetime(2010, 8, 6, 12, 0, 0)
		logger.debug("Calibration dates: {}".format(calibration_dates_ints))
		del calibration_dates_ints[0]
		calibration_dates = map(_db_format_to_datetime, calibration_dates_ints)
		for datetime_, datetime_int_ in zip(calibration_dates, calibration_dates_ints):
			tempath1 = "{}/{}".format(tmp_dump_loc, datetime_.date())
			logger.debug("Path:{} Cons:{}".format(tempath1, db.path))
			logger.debug("Dumping data with timestamp %d (%s)" % (datetime_int_, datetime_.date()))
			DumpToFiles(db, datetime_int_, version_tag, ["/"], tempath1, '') # dumping xmls using condb engine
			move_to_dir(tempath1, destination)
	clean(tmp_dump_loc)

def clean(directory):
	"""Removes temporary working directory."""
	try:
		error = os.system('rm -rf ' + directory)
	except:
		print "ERROR: Something went wrong during cleaning after VV session: deleting /tmp/VeloView catalog failed."
	if error:
		print "ERROR: Something went wrong during cleaning after VV session: deleting /tmp/VeloView catalog failed."

if __name__=='__main__':
	parser = argparse.ArgumentParser(description='Connect to VELOCONDDB and dump all tell1 parameters to xml (pedestals, low and high treshold).')
	parser.add_argument('destination', type=str, help='path to destination for created xml files')
	args = parser.parse_args()
	dump_to_dir(args.destination)
