import os
import argparse
from datetime import datetime
import glob
from xml.etree import ElementTree
from ROOT import TGraph, TCanvas, TFile, TH1D, TDirectory
import logging
import csv

logging.basicConfig()
logger = logging.getLogger('VeloAnalysisFramework.scripts.tell1_calib_params_root')
logger.setLevel(logging.DEBUG)

def folder_name_to_date(folder_name):
	return datetime.strptime(folder_name, '%Y-%m-%d')

def date_to_tree_path(run_date):
	return run_date.strftime('%Y/%m/%d')

def set_graph_pedestal(values, graph):
	for i, value in enumerate(values):
		graph.AddBinContent(i, value)
	yaxis = graph.GetYaxis()
	yaxis.SetRangeUser(460, 580)
	yaxis.SetTitle('pedestal')
	return graph

def set_graph_hit_threshold(values, graph):
	for i, value in enumerate(values):
		graph.AddBinContent(i + 1, value)
	yaxis = graph.GetYaxis()
	yaxis.SetRangeUser(0, 140)
	yaxis.SetTitle('hit_threshold')
	return graph

def set_graph_low_threshold(values, graph):
	for i, value in enumerate(values):
		graph.AddBinContent(i + 1, value)
	yaxis = graph.GetYaxis()
	yaxis.SetRangeUser(0, 140)
	yaxis.SetTitle('low_threshold')
	return graph

graphs_dict = {
	'pedestal': {
		'settings': set_graph_pedestal,
		'root_name': 'pedestal_sum'
	},
	'hit_threshold': {
		'settings': set_graph_hit_threshold,
		'root_name': 'hit_threshold'
	},
	'low_threshold': {
		'settings': set_graph_low_threshold,
		'root_name': 'low_threshold'
	}
}


def generate_data_from_xml(xml_files, name):
	for file_path in xml_files:
		file_name = os.path.basename(file_path).split('.')[0]
		tree = ElementTree.parse(file_path)
		root = tree.getroot()
		for param_vector in root.iter('paramVector'):
			if param_vector.attrib['name'] != graphs_dict[name]['root_name']:
				continue
			values = [int(x) for x in param_vector.text.split()]
			yield file_name, values

def make_root(xml_files, f, name):
	cdtof = f.mkdir(name)
	cdtof.cd()
	for file_name, values in generate_data_from_xml(xml_files, name):
		length = len(values)
		graph = TH1D(file_name, file_name, length, 1, 1+length)
		graph = graphs_dict[name]['settings'](values, graph)
		xaxis = graph.GetXaxis()
		xaxis.SetTitle('channel number')
		graph.Write()

def make_csv(xml_files, f, name):
	for file_name, values in generate_data_from_xml(xml_files, name):
		writer = csv.writer(f, delimiter=' ')
		writer.writerow([name, file_name] + values)


class TFileWrapper(TFile):
	def __init__(self, *args, **kwargs):
		self.args = args
		self.kwargs = kwargs

	def __enter__(self):
		TFile.__init__(self, *self.args, **self.kwargs)
		return self

	def __exit__(self, type, value, traceback):
		self.Close()

filetype_api = {
	'file_openers':{
		'root': lambda x : TFileWrapper(x, 'Recreate'),
		'csv': lambda x : open(x, 'w')
	},
	'dump_maker':{
		'root': make_root,
		'csv': make_csv
	}
}

def make_dumps(home_path_source,folder_name, path_to_new_folder, filetype):
	xml_files_pattern = os.path.join(home_path_source, '{}/*.xml'.format(folder_name))
	xml_files = glob.glob(xml_files_pattern)
	if not os.path.isdir(path_to_new_folder):
		os.makedirs(path_to_new_folder)
	dump_filename = '{}.{}'.format(folder_name, filetype)
	dump_file_path = os.path.join(path_to_new_folder, dump_filename)
	logger.debug('Created new {}: {}'.format(folder_name, filetype))
	with filetype_api['file_openers'][filetype](dump_file_path) as f:
		for graph_name in graphs_dict:
			filetype_api['dump_maker'][filetype](xml_files, f, graph_name)

def add_new_run(home_path_source, folder_name, home_path_destination, file_type, plain_dump=False):
	date = folder_name_to_date(folder_name)
	date_path = '' if plain_dump else date_to_tree_path(date)
	path_to_new_folder = os.path.join(home_path_destination, date_path)
	make_dumps(home_path_source, folder_name, path_to_new_folder, file_type)

def append_to_run_list(home_path_destination, folder_name):
	run_list_path = os.path.join(home_path_destination, 'RunList.txt')
	with open(run_list_path,'a') as run_list_file:
		run_line = '{}\n'.format(folder_name)
		run_list_file.write(run_line)
		logger.debug('Adding line {} to runlist file at :{}'.format(run_line,run_list_path))

def add_all_run(home_path_source, home_path_destination, file_type, plain_dump=False, run_list=False):
	folders = os.listdir(home_path_source)
	logger.debug('folders {}'.format(folders))
	logger.debug('destination: {}'.format(home_path_destination))
	for folder_name in folders:
		if run_list:
			append_to_run_list(home_path_destination, folder_name)
		add_new_run(home_path_source, folder_name, home_path_destination, file_type, plain_dump)

if __name__=='__main__':
	parser = argparse.ArgumentParser(description='Create dump files from velocond dumps')
	parser.add_argument('veloconddb_files', type=str, help='path to veloconddb dump')
	parser.add_argument('destination', type=str, help='path to destination for created root files')
	parser.add_argument('--csv', dest='csv', action='store_true', help='dump as csv')
	parser.add_argument('--root', dest='root', action='store_true', help='dump to root')
	parser.add_argument('--plain_dump', dest='plain_dump', action='store_true', help='no catalogue tree, dump all to one location')
	parser.add_argument('--run_list', dest='runlist', action='store_true', help='add run list in destination folder')
	args = parser.parse_args()
	home_path_source_test = args.veloconddb_files
	home_path_destination_test = args.destination
	run_list = args.runlist
	if  args.root:
		add_all_run(home_path_source_test, home_path_destination_test, 'root', plain_dump=args.plain_dump, run_list=run_list)
	if args.csv:
		add_all_run(home_path_source_test, home_path_destination_test, 'csv', plain_dump=args.plain_dump, run_list=run_list)

