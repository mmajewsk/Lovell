# coding=utf-8
"""This module will hold complex comparison functions"""

from logging import debug

from ..core.interface import ComparisonFunction, check_hists
from ..core.score_manipulation import Score
from ..utils.rootutils import frac_above_threshold, frac_below_threshold

def calc_score(value, minimum, maximum, tolerance):
    """Calculate a score smoothed using a certain tolerance."""
    # For now this is linear, maybe smoothe it out in the future
    if minimum is None and maximum is None:
        return Score.MAX
    if value < minimum:
        return max(Score.MIN, (Score.MAX - Score.MIN) * (-minimum + tolerance + value) / tolerance)
    elif value > maximum:
        return max(Score.MIN, (Score.MAX - Score.MIN) * ( maximum + tolerance - value) / tolerance)
    return Score.MAX

class MeanRmsThreshold(ComparisonFunction):
    @classmethod
    @check_hists
    def compare(cls, data_hist, ref_hist, params):
        mode, axis, minMean, maxMean, minRms, maxRms, tolerance = params
        assert(mode in [0, 1, 2])

        if mode == 0:
            meanValue = data_hist.GetMean(axis)
            rmsValue = data_hist.GetRMS(axis)
        elif mode == 1:
            data_clone = data_hist.Clone()
            data_clone.Add(ref_hist, -1.)
            meanValue = data_clone.GetMean(axis)
            rmsValue = data_clone.GetRMS(axis)
        elif mode == 2:
            meanValue = data_hist.GetMean(axis) - ref_hist.GetMean(axis)
            rmsValue = data_hist.GetRMS(axis) - ref_hist.GetRMS(axis)

        meanScore = calc_score(meanValue, minMean, maxMean, tolerance)
        rmsScore  = calc_score(rmsValue,  minRms,  maxRms,  tolerance)
        return Score(min(meanScore, rmsScore))

class MeanThresholdRPhi(ComparisonFunction):
    """Mean calculation for R or Phi only"""
    @classmethod
    @check_hists
    def compare(cls, data_hist, ref_hist, params):
        mode, rPhi, minMean, maxMean, tolerance = params
        assert(mode in [0, 2] and rPhi in ['R', 'Phi'])
        total = n = 0
        for hist_bin in xrange(data_hist.GetSize()):
            if data_hist.IsBinOverflow(hist_bin) or data_hist.IsBinUnderflow(hist_bin):
                continue
            center = data_hist.GetBinCenter(hist_bin)
            if (rPhi == 'R' and 0 < center < 42) or (rPhi == 'Phi' and 63 < center < 106):
                total += data_hist.GetBinContent(hist_bin)
                if mode == 2:
                    total -= ref_hist.GetBinContent(hist_bin)
                n += 1
        return Score(calc_score(1. * total / n, minMean, maxMean, tolerance))

class MeanThresholdAC(ComparisonFunction):
    """Separate calculations for A and C side"""
    @classmethod
    @check_hists
    def compare(cls, data_hist, ref_hist, params):
        delta = .0000001
        minMean, maxMean, tolerance = params
        sumA = nA = sumC = nC = 0
        for hist_bin in xrange(data_hist.GetSize()):
            value = data_hist.GetBinContent(hist_bin)
            if data_hist.IsBinOverflow(hist_bin) or data_hist.IsBinUnderflow(hist_bin):
                continue
            if value < -delta:
                sumC += value
                nC += 1
            elif value > delta:
                sumA += value
                nA += 1
        scoreA = calc_score(1. * sumA / nA, minMean, maxMean, tolerance)
        scoreC = calc_score(1. * sumC / nC, minMean, maxMean, tolerance)
        return Score(min(scoreA, scoreC))

class CountThreshold(ComparisonFunction):
    """Check how many bins fulfill a criterium.
    mode can be 0 (check data) or 1 (check data - ref)
    """
    @classmethod
    @check_hists
    def compare(cls, data_hist, ref_hist, params):
        operator, minimum, maximum, tolerance, mode = params
        assert(mode in [0, 1] and isinstance(operator(0.), bool))
        n = 0
        for hist_bin in xrange(data_hist.GetSize()):
            dataValue = data_hist.GetBinContent(hist_bin)
            if mode == 0:
                value = dataValue
            else:
                value = dataValue - ref_hist.GetBinContent(hist_bin)

            if data_hist.IsBinOverflow(hist_bin) or data_hist.IsBinUnderflow(hist_bin):
                continue
            if operator(value):
                n += 1

        return Score(calc_score(n, minimum, maximum, tolerance))

class FractionThreshold(ComparisonFunction):
    """Check which fraction of bins fall outside a band.
    mode can be 0 (check data) or 1 (check data - ref)
    """
    @classmethod
    @check_hists
    def compare(cls, data_hist, ref_hist, params):
        mode, minimum, maximum, minFraction, maxFraction, tolerance = params
        assert(mode in [0, 1])
        hist = data_hist if mode == 0 else data_hist - ref_hist

        frac_above = frac_below = 0.
        emptyhist = True
	if hist.GetEntries() > 0.5:
            emptyhist = False
        if (maximum is not None) and (emptyhist == False):
            frac_above = frac_above_threshold(hist, maximum)
        if (minimum is not None) and (emptyhist == False):
            frac_below = frac_below_threshold(hist, minimum)
        return Score(calc_score(frac_below + frac_above, minFraction, maxFraction, tolerance))

class IntegralThreshold(ComparisonFunction):
    """Check the integral of the histogram against thresholds."""
    @classmethod
    @check_hists
    def compare(cls, data_hist, ref_hist, params):
        minimum, maximum, tolerance, mode = params
        assert(mode in [0, 2])
        if mode == 0:
            integral = data_hist.Integral()
        elif mode == 2:
            integral = data_hist.Integral() - ref_hist.Integral()
        return Score(calc_score(integral, minimum, maximum, tolerance))

