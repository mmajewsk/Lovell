# -*- coding: utf-8 -*-
"""Methods required for obtained and anaylsing the IV scans

Responsible: Cameron Dean
"""
from array import array
from datetime import datetime
import glob
import os
import numpy

import ROOT

from veloview.config import Config
from veloview.runview import response_formatters

IV_DATA_EXT = '.iv'

def scaling_params():
    rescale_temp = -10
    overview_volt = 200
    return (rescale_temp, overview_volt)

def iv_list():
    """Return a list of all available IV scans."""
    prefix = Config().iv_data_dir
    scans = glob.glob(os.path.join(prefix, '*' + IV_DATA_EXT))
    scans = [s.replace(prefix, '').replace(IV_DATA_EXT, '')[1:] for s in scans]
    return scans


def iv_datetime_to_object(iv_datetime):
    try:
        dt = datetime.strptime(iv_datetime, '%y%m%d_%H%M')
    except ValueError:
        dt = datetime.utcfromtimestamp(0)
    return dt


def iv_datetime_list():
    """Return a list of 2-tuples of all availble (IV scan, datetime)."""
    # Valid files start with a date, beginning with the last two digits of the
    # year, so we requires filenames to start with 0 or 1, for years in the
    # 2000s and 2010s
    scans = [s for s in iv_list() if s.startswith(('0', '1'))]
    datetimes = ['_'.join(s.split('_')[:2]) for s in scans]
    datetimes = map(iv_datetime_to_object, datetimes)
    return zip(scans, datetimes)


def sensor_conv(sensor_id):
    """Map the sensor number to the appropriate ISEG channel."""
    iv_conv_path = os.path.join(Config().iv_data_dir, Config().iv_conv_list)
    sens_conv = open(iv_conv_path, 'r')
    sensor_str = str(sensor_id) + ' '
    # Guard against the sensor not having a matching module
    name = 'name-not-found'
    channel = 'module-not-found'
    position = 'position-not-found'
    fluence = 'fluence-not-found'
    for line in sens_conv:
        if sensor_str in line:
            conversion = line.split()
            name = conversion[2]
            channel = conversion[3]
            position = conversion[4]
            fluence = conversion[5]
    nameString = "".join(name)
    channelString = "".join(channel)
    positionString = "".join(position)
    fluenceString = "".join(fluence)
    return (nameString, channelString, positionString, fluenceString)


def sensor_name(sensor_id):
    sensor_info = sensor_conv(sensor_id)
    return sensor_info[0]


def sensor_channel(sensor_id):
    sensor_info = sensor_conv(sensor_id)
    return sensor_info[1]


def sensor_position(sensor_id):
    sensor_info = sensor_conv(sensor_id)
    return sensor_info[2]


def sensor_fluence(sensor_id):
    sensor_info = sensor_conv(sensor_id)
    return sensor_info[3]


def sensor_order():
    a_side = [128,64,0,2,66,68,4,6,70,72,8,10,74,76,12,14,78,80,16,18,82,84,20,22,86,88,24,26,90,92,28,30,94,96,32,34,98,100,36,38,102,104,40]
    c_side = []
    c_side[:] = [i + 1 for i in a_side]
    return (a_side, c_side)


def iv_plot(x_arr, y_arr, x_label, y_label):
    plot = ROOT.TGraph(len(x_arr), x_arr, y_arr)
    plot.GetXaxis().SetTitle(x_label)
    plot.GetYaxis().SetTitle(y_label)
    formatted_plot = response_formatters.tobject_formatter(plot)
    return formatted_plot


def iv_histo(x_arr, x_label):
    histo = ROOT.TH1F("","",len(x_arr),min(x_arr),max(x_arr))
    for i in range(len(x_arr)):
        histo.Fill(x_arr[i])
    histo.GetXaxis().SetTitle(x_label)
    formatted_histo = response_formatters.tobject_formatter(histo)
    return formatted_histo

def it_scale(temp):
    kelvin = 273.15
    band_gap = -1.21
    boltzmann = 8.62*pow(10, -5)
    i_conv = pow((kelvin+temp),2)*numpy.exp(band_gap/(2*boltzmann*(kelvin+temp)))
    return i_conv


def iv_data(file, sensor_id):
    """Return data from the given IV scan file for the sensor for the GUIs."""
    volt = []
    curr = []
    temp = []
    if not file.endswith(IV_DATA_EXT):
        file += IV_DATA_EXT
    module = sensor_conv(sensor_id)
    file_path = os.path.join(Config().iv_data_dir, file)
    f = open(file_path, 'r')
    for data_point in f:
        if module[1] in data_point:
            plot_values = data_point.split()
            volt.append(float(plot_values[2]))
            curr.append(float(plot_values[3]))
            temp.append(float(plot_values[5]))

    # TGraph won't instantiate with an empty array
    if not volt:
        volt = [0]
    if not curr:
        curr = [0]

    v_arr = array('f', volt)
    i_arr = array('f', curr)
    t_avg = numpy.mean(temp)

    iv_plot_act = iv_plot(v_arr, i_arr , 'Bias voltage [V]', 'Current [µA]')
    return (iv_plot_act, t_avg)


def iv_data_corrected(file, sensor_id):
    """Return data from the given IV scan file for the sensor for the GUIs and perform a temperature correction"""
    volt = []
    curr = []
    temp = []
    i_scaled = []
    rescale_param = scaling_params()
    rescale_temp = rescale_param[0]
    rescale_curr = it_scale(rescale_temp)
    if not file.endswith(IV_DATA_EXT):
        file += IV_DATA_EXT
    module = sensor_conv(sensor_id)
    file_path = os.path.join(Config().iv_data_dir, file)
    f = open(file_path, 'r')
    for data_point in f:
        if module[1] in data_point:
            plot_values = data_point.split()
            volt.append(float(plot_values[2]))
            curr.append(float(plot_values[3]))
            temp.append(float(plot_values[5]))

    # TGraph won't instantiate with an empty array
    if not volt:
        volt = [0]
    if not curr:
        curr = [0]

    v_arr = array('f', volt)
    i_arr = array('f', curr)
    for i in range (0, len(temp)):
        i_scaled.append(float(i_arr[i]*rescale_curr/it_scale(temp[i])))

    if not i_scaled:
        i_scaled = [0]

    i_scaled_arr = array('f', i_scaled)
    y_axis = 'Scaled current [µA]'.format(rescale_temp)

    iv_plot_corrected = iv_plot(v_arr, i_scaled_arr, 'Bias voltage [V]', y_axis)
    return iv_plot_corrected


def iv_data_all(file):
    rescale_param = scaling_params()
    overview_volt = rescale_param[1]
    sensors = sensor_order()
    a_channels = []
    a_positions = []
    a_currs = []
    a_temps = []
    a_fluences = []
    c_channels = []
    c_positions = []
    c_currs = []
    c_temps = []
    c_fluences = []
    for (i,j) in zip(sensors[0], sensors[1]):
        a_channels.append(sensor_channel(i) + '       {0:.1f}'.format(overview_volt))
        a_positions.append(float(sensor_position(i)))
        a_fluences.append(float(sensor_fluence(i)))
        c_channels.append(sensor_channel(j) + '       {0:.1f}'.format(overview_volt))
        c_positions.append(float(sensor_position(j)))
        c_fluences.append(float(sensor_fluence(j)))
    if not file.endswith(IV_DATA_EXT):
        file += IV_DATA_EXT
    file_path = os.path.join(Config().iv_data_dir, file)
    for (k,l) in zip(a_channels, c_channels):
        f = open(file_path, 'r')
        for data_point in f:
            if k in data_point:
                a_plot_values = data_point.split()
                a_currs.append(float(a_plot_values[3]))
                a_temps.append(float(a_plot_values[5]))
        f = open(file_path, 'r')
        for data_point in f:
            if l in data_point:
                c_plot_values = data_point.split()
                c_currs.append(float(c_plot_values[3]))
                c_temps.append(float(c_plot_values[5]))
    # TGraph won't instantiate with an empty array
    if not a_positions:
        a_positions = [0]
    if not a_currs:
        a_currs = [0]
    if not c_positions:
        c_positions = [0]
    if not c_currs:
        c_currs = [0]
    a_positions_arr = array('f', a_positions)
    a_currs_arr = array('f', a_currs)
    c_positions_arr = array('f', c_positions)
    c_currs_arr = array('f', c_currs)
    return (a_positions_arr, a_currs_arr, a_temps, a_fluences, c_positions_arr, c_currs_arr, c_temps, c_fluences)


def iv_scan(iv_file, ref_file, sensor_id):
    data = iv_data(iv_file, sensor_id)
    data_iv = data[0]
    data_temp = data[1]
    if ref_file is not None:
        reference = iv_data(ref_file, sensor_id)
        ref_iv = reference[0]
        ref_temp = reference[1]
    else:
        ref_iv = None
        ref_temp = None
    return (data_iv, ref_iv, data_temp, ref_temp)


def iv_scan_corrected(iv_file, ref_file, sensor_id):
    data = iv_data_corrected(iv_file, sensor_id)
    if ref_file is not None:
        reference = iv_data_corrected(ref_file, sensor_id)
    else:
        reference = None
    return (data, reference)


def iv_overview(iv_file, ref_file):
    rescale_param = scaling_params()
    overview_volt = rescale_param[1]
    x_label = 'Position [mm]'
    y_label = 'Current [µA] at {0:.1f} V'.format(overview_volt)

    data = iv_data_all(iv_file)
    a_overview_plot = iv_plot(data[0], data[1], x_label, y_label)
    c_overview_plot = iv_plot(data[4], data[5], x_label, y_label)
    a_overview_histo = iv_histo(data[1], y_label)
    c_overview_histo = iv_histo(data[5], y_label)

    if ref_file is not None:
        reference = iv_data_all(ref_file)
        a_overview_plot_ref = iv_plot(reference[0], reference[1], x_label, y_label)
        c_overview_plot_ref = iv_plot(reference[4], reference[5], x_label, y_label)
        a_overview_histo_ref = iv_histo(reference[1], y_label)
        c_overview_histo_ref = iv_histo(reference[5], y_label)
    else:
        a_overview_plot_ref = None
        c_overview_plot_ref= None
        a_overview_histo_ref = None
        c_overview_histo_ref= None

    return (a_overview_plot, c_overview_plot, a_overview_plot_ref, c_overview_plot_ref, a_overview_histo, c_overview_histo, a_overview_histo_ref, c_overview_histo_ref)


def iv_overview_temp(iv_file, ref_file):
    rescale_param = scaling_params()
    rescale_temp = rescale_param[0]
    overview_volt = rescale_param[1]
    x_label = 'Position [mm]'
    y_label = 'Current [µA] at {0:.1f} V'.format(overview_volt)
    y_label += ' and {0:.1f} °C'.format(rescale_temp)
    rescale_curr = it_scale(rescale_temp)
    data = iv_data_all(iv_file)
    a_currs = data[1]
    a_temps = data[2]
    c_currs = data[5]
    c_temps = data[6]
    a_currs_scaled = []
    c_currs_scaled = []
    a_currs_scaled_ref = []
    c_currs_scaled_ref = []
    for i in range (0, len(a_temps)):
        a_currs_scaled.append(float(a_currs[i]*rescale_curr/it_scale(a_temps[i])))
        c_currs_scaled.append(float(c_currs[i]*rescale_curr/it_scale(c_temps[i])))

    if not a_currs_scaled:
        a_currs_scaled = [0]
    if not c_currs_scaled:
        c_currs_scaled = [0]

    a_currs_scaled_arr = array('f', a_currs_scaled)
    c_currs_scaled_arr = array('f', c_currs_scaled)
    a_overview_plot_temp = iv_plot(data[0], a_currs_scaled_arr, x_label, y_label)
    c_overview_plot_temp = iv_plot(data[4], c_currs_scaled_arr, x_label, y_label)
    a_overview_histo_temp = iv_histo(a_currs_scaled_arr, y_label)
    c_overview_histo_temp = iv_histo(c_currs_scaled_arr, y_label)

    if ref_file is not None:
        reference = iv_data_all(ref_file)
        a_currs_ref = reference[1]
        a_temps_ref = reference[2]
        c_currs_ref = reference[5]
        c_temps_ref = reference[6]
        for j in range (0, len(a_temps_ref)):
            a_currs_scaled_ref.append(float(a_currs_ref[j]*rescale_curr/it_scale(a_temps_ref[j])))
            c_currs_scaled_ref.append(float(c_currs_ref[j]*rescale_curr/it_scale(c_temps_ref[j])))

        if not a_currs_scaled_ref:
            a_currs_scaled_ref = [0]
        if not c_currs_scaled_ref:
            c_currs_scaled_ref = [0]

        a_currs_scaled_arr_ref = array('f', a_currs_scaled)
        c_currs_scaled_arr_ref = array('f', c_currs_scaled)
        a_overview_plot_temp_ref = iv_plot(reference[0], a_currs_scaled_arr_ref, x_label, y_label)
        c_overview_plot_temp_ref = iv_plot(reference[4], c_currs_scaled_arr_ref, x_label, y_label)
        a_overview_histo_temp_ref = iv_histo(a_currs_scaled_arr_ref, y_label)
        c_overview_histo_temp_ref = iv_histo(c_currs_scaled_arr_ref, y_label)
    else:
        a_overview_plot_temp_ref = None
        c_overview_plot_temp_ref= None
        a_overview_histo_temp_ref = None
        c_overview_histo_temp_ref = None

    return (a_overview_plot_temp, c_overview_plot_temp, a_overview_plot_temp_ref, c_overview_plot_temp_ref, a_overview_histo_temp, c_overview_histo_temp, a_overview_histo_temp_ref, c_overview_histo_temp_ref)


def iv_overview_fluence(iv_file, ref_file):
    rescale_param = scaling_params()
    rescale_temp = rescale_param[0]
    overview_volt = rescale_param[1]
    x_label = 'Position [mm]'
    y_label = 'Current [µA] at {0:.1f} V'.format(overview_volt)
    y_label += ' and {0:.1f} °C, irradtion corrected'.format(rescale_temp)
    rescale_curr = it_scale(rescale_temp)
    data = iv_data_all(iv_file)
    a_currs = data[1]
    a_temps = data[2]
    a_fluences = data[3]
    c_currs = data[5]
    c_temps = data[6]
    c_fluences = data[7]
    a_currs_corrected = []
    c_currs_corrected = []
    a_currs_corrected_ref = []
    c_currs_corrected_ref = []
    for i in range (0, len(a_temps)):
        a_currs_corrected.append(float(a_currs[i]*rescale_curr/(it_scale(a_temps[i])*a_fluences[i])))
        c_currs_corrected.append(float(c_currs[i]*rescale_curr/(it_scale(c_temps[i])*c_fluences[i])))

    if not a_currs_corrected:
        a_currs_corrected = [0]
    if not c_currs_corrected:
        c_currs_corrected = [0]

    a_currs_corrected_arr = array('f', a_currs_corrected)
    c_currs_corrected_arr = array('f', c_currs_corrected)
    a_overview_plot_fluence = iv_plot(data[0], a_currs_corrected_arr, x_label, y_label)
    c_overview_plot_fluence = iv_plot(data[4], c_currs_corrected_arr, x_label, y_label)
    a_overview_histo_fluence = iv_histo(a_currs_corrected_arr, y_label)
    c_overview_histo_fluence = iv_histo(c_currs_corrected_arr, y_label)

    if ref_file is not None:
        reference = iv_data_all(ref_file)
        a_currs_ref = reference[1]
        a_temps_ref = reference[2]
        a_fluences_ref = reference[3]
        c_currs_ref = reference[5]
        c_temps_ref = reference[6]
        c_fluences_ref = reference[7]
        for j in range (0, len(a_temps_ref)):
            a_currs_corrected_ref.append(float(a_currs_ref[j]*rescale_curr/(it_scale(a_temps_ref[j])*a_fluences_ref[j])))
            c_currs_corrected_ref.append(float(c_currs_ref[j]*rescale_curr/(it_scale(c_temps_ref[j])*c_fluences_ref[j])))

        if not a_currs_corrected_ref:
            a_currs_corrected_ref = [0]
        if not c_currs_corrected_ref:
            c_currs_corrected_ref = [0]

        a_currs_corrected_arr_ref = array('f', a_currs_corrected_ref)
        c_currs_corrected_arr_ref = array('f', c_currs_corrected_ref)
        a_overview_plot_fluence_ref = iv_plot(reference[0], a_currs_corrected_arr_ref, x_label, y_label)
        c_overview_plot_fluence_ref = iv_plot(reference[4], c_currs_corrected_arr_ref, x_label, y_label)
        a_overview_histo_fluence_ref = iv_histo(a_currs_corrected_arr_ref, y_label)
        c_overview_histo_fluence_ref = iv_histo(c_currs_corrected_arr_ref, y_label)
    else:
        a_overview_plot_fluence_ref = None
        c_overview_plot_fluence_ref= None
        a_overview_histo_fluence_ref = None
        c_overview_histo_fluence_ref = None

    return (a_overview_plot_fluence, c_overview_plot_fluence, a_overview_plot_fluence_ref, c_overview_plot_fluence_ref, a_overview_histo_fluence, c_overview_histo_fluence, a_overview_histo_fluence_ref, c_overview_histo_fluence_ref)
