import os
import tempfile
import shutil

from veloview.core.analysis_config_wrapper import AnalysisConfigWrapper
from veloview.core.score_manipulation import ERROR_LEVELS
from veloview.core.combiners import BranchCombiner

# aliases
OK = ERROR_LEVELS.OK
WARNING = ERROR_LEVELS.WARNING
ERROR = ERROR_LEVELS.ERROR

from ROOT import TFile
import unittest

class TestAvgHistCombiners(unittest.TestCase):

    def setUp(self):
        """Create dictionaries needed by combiners."""

        dirname = os.path.join(os.path.dirname(__file__), 'fixtures')
        orfdata = TFile(
            os.path.abspath(os.path.join(dirname, 'dqm_data.root')),
            'read'
        )
        orfref = TFile(
            os.path.abspath(os.path.join(dirname, 'dqm_ref.root')),
            'read'
        )

        # valid ROOT files
        assert(not orfdata.IsZombie())
        assert(not orfref.IsZombie())

        configfile = os.path.abspath(os.path.join(os.path.dirname(__file__), 'analysis_config_test.py'))
        with open(configfile, 'r') as inputFile:
            exec(inputFile.read())
        config = AnalysisConfigWrapper((analysis_config_branches, analysis_config_leaves))
        self.mycombiner = config.getTrunk(orfdata.GetName(), orfref.GetName(), r"noise\S*")
        self.mycombiner.evaluate()

    def tearDown(self):
        del self.mycombiner

    def for_each_combiner(self, combiner, res):
        """Call test on each node/leaf of combiner."""

        if isinstance(combiner, BranchCombiner):
            for child_combiner in combiner.get_children():
                self.for_each_combiner(child_combiner, res)
        # append my result
        res.append((combiner.get_name(), combiner.get_level()))
        return res

    def test_combiners(self):
        """Test all combiners recursively with real-life monitoring files"""

        res = self.for_each_combiner(self.mycombiner, [])
        self.maxDiff = None
        self.assertSequenceEqual(sorted(res),\
            sorted([('MasterCombiner', OK), ('noise', OK), ('noise_per_link', OK), ('noise_per_link_direct', OK), ('noise_per_link_ref', OK), ('noise_per_station', OK), ('noise_per_station_phi', OK), ('noise_per_station_r', OK), ('noise_per_sensor', OK), ('noise_per_sensor_direct', OK), ('noise_per_sensor_direct_064', OK), ('noise_per_sensor_direct_065', OK), ('noise_per_sensor_direct_066', OK), ('noise_per_sensor_direct_067', OK), ('noise_per_sensor_direct_068', OK), ('noise_per_sensor_direct_069', OK), ('noise_per_sensor_direct_070', OK), ('noise_per_sensor_direct_071', OK), ('noise_per_sensor_direct_072', OK), ('noise_per_sensor_direct_073', OK), ('noise_per_sensor_direct_074', OK), ('noise_per_sensor_direct_075', OK), ('noise_per_sensor_direct_076', OK), ('noise_per_sensor_direct_077', OK), ('noise_per_sensor_direct_078', OK), ('noise_per_sensor_direct_079', OK), ('noise_per_sensor_direct_080', OK), ('noise_per_sensor_direct_081', OK), ('noise_per_sensor_direct_082', OK), ('noise_per_sensor_direct_083', OK), ('noise_per_sensor_direct_084', OK), ('noise_per_sensor_direct_085', OK), ('noise_per_sensor_direct_086', OK), ('noise_per_sensor_direct_087', OK), ('noise_per_sensor_direct_088', OK), ('noise_per_sensor_direct_089', OK), ('noise_per_sensor_direct_090', OK), ('noise_per_sensor_direct_091', OK), ('noise_per_sensor_direct_092', OK), ('noise_per_sensor_direct_093', OK), ('noise_per_sensor_direct_094', OK), ('noise_per_sensor_direct_095', OK), ('noise_per_sensor_direct_096', OK), ('noise_per_sensor_direct_097', OK), ('noise_per_sensor_direct_098', OK), ('noise_per_sensor_direct_099', OK), ('noise_per_sensor_direct_100', OK), ('noise_per_sensor_direct_101', OK), ('noise_per_sensor_direct_102', OK), ('noise_per_sensor_direct_103', OK), ('noise_per_sensor_direct_104', OK), ('noise_per_sensor_direct_105', OK), ('noise_per_sensor_direct_000', OK), ('noise_per_sensor_direct_001', OK), ('noise_per_sensor_direct_002', OK), ('noise_per_sensor_direct_003', OK), ('noise_per_sensor_direct_004', OK), ('noise_per_sensor_direct_005', OK), ('noise_per_sensor_direct_006', OK), ('noise_per_sensor_direct_007', OK), ('noise_per_sensor_direct_008', OK), ('noise_per_sensor_direct_009', OK), ('noise_per_sensor_direct_010', OK), ('noise_per_sensor_direct_011', OK), ('noise_per_sensor_direct_012', OK), ('noise_per_sensor_direct_013', OK), ('noise_per_sensor_direct_014', OK), ('noise_per_sensor_direct_015', OK), ('noise_per_sensor_direct_016', OK), ('noise_per_sensor_direct_017', OK), ('noise_per_sensor_direct_018', OK), ('noise_per_sensor_direct_019', OK), ('noise_per_sensor_direct_020', OK), ('noise_per_sensor_direct_021', OK), ('noise_per_sensor_direct_022', OK), ('noise_per_sensor_direct_023', OK), ('noise_per_sensor_direct_024', OK), ('noise_per_sensor_direct_025', OK), ('noise_per_sensor_direct_026', OK), ('noise_per_sensor_direct_027', OK), ('noise_per_sensor_direct_028', OK), ('noise_per_sensor_direct_029', OK), ('noise_per_sensor_direct_030', OK), ('noise_per_sensor_direct_031', OK), ('noise_per_sensor_direct_032', OK), ('noise_per_sensor_direct_033', OK), ('noise_per_sensor_direct_034', OK), ('noise_per_sensor_direct_035', OK), ('noise_per_sensor_direct_036', OK), ('noise_per_sensor_direct_037', OK), ('noise_per_sensor_direct_038', OK), ('noise_per_sensor_direct_039', OK), ('noise_per_sensor_direct_040', OK), ('noise_per_sensor_direct_041', OK)]))

if __name__ == '__main__':
    unittest.main()
